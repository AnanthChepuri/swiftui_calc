//
//  ContentView.swift
//  Calc
//
//  Created by Ananth Chepuri on 07/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var firstnumber = 0
    @State var secondnumber = 0
    @State var calcText = ""
    @State var isTypingNumber = false
    @State var operatorType = ""
    
    var body: some View {
        VStack (alignment: .center, spacing: 10) {

            TextField("", text: self.$calcText)
                .multilineTextAlignment(.trailing)
                .frame(height: 20, alignment: .center)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 14)
                        .stroke(Color.gray, lineWidth: 1)
                )
            
            //CalcButton.calcButton("Hi")
            Divider()
            HStack {
                Button("7") {
                    self.digitTapped("7")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("8") {
                    self.digitTapped("8")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("9") {
                    self.digitTapped("9")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("x") {
                    self.operandTapped("x")
                }.buttonStyle(MyButtonStyle(color: .blue))
            }
            
            HStack {
                Button("4") {
                    self.digitTapped("4")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("5") {
                    self.digitTapped("5")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("6") {
                    self.digitTapped("6")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("-") {
                    self.operandTapped("-")
                }.buttonStyle(MyButtonStyle(color: .blue))
            }
            
            HStack {
                Button("1") {
                    self.digitTapped("1")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("2") {
                    self.digitTapped("2")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("3") {
                    self.digitTapped("3")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("+") {
                    self.operandTapped("+")
                }.buttonStyle(MyButtonStyle(color: .blue))
            }

            HStack {
                Button("Clear") {
                    self.clear()
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("0") {
                    self.digitTapped("0")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button(".") {
                    print("button pressed!")
                }.buttonStyle(MyButtonStyle(color: .blue))
                
                Button("=") {
                    self.calculate()
                }.buttonStyle(MyButtonStyle(color: .blue))
            }
            
//            HStack {
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("1")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("2")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("3")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("X")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//            }
//            HStack {
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("4")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("5")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("6")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("-")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//            }
//            HStack {
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("7")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("8")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("9")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//                Button(action: {
//                    // What to perform
//                    self.clickMe()
//                }) {
//                    // How the button looks like
//                    Text("+")
//                    .font(.headline)
//                    .foregroundColor(.white)
//                    .padding()
//                        .frame(width: 80, height: 50, alignment: .center)
//                    .background(Color.green)
//                    .cornerRadius(14.0)
//                }
//            }
            
//            HStack {
//                ForEach(0 ..< 3) { number in
//                    Button(action: {
//                       // flag was tapped
//                    }) {
//                        Text("\(number)")
//                            .frame(width: 80, height: 50, alignment: .center)
//                            .font(.headline)
//                            .foregroundColor(.white)
//                            .background(Color.green)
//                            .cornerRadius(14.0)
//                    }
//                }
//            }
            
            
            
            Spacer()
        }
        .padding([.leading, .trailing], 20)
    }
    
    func digitTapped(_ number: String) -> Void {
        if isTypingNumber {
            calcText += number
        } else {
            calcText = number
            isTypingNumber = true
        }
    }
    
    func operandTapped(_ operand: String) {
        isTypingNumber = false
        firstnumber = Int(calcText)!
        self.operatorType = operand
        calcText = operand
    }
    
    func calculate() {
        isTypingNumber = false
        var result = 0
        if secondnumber == 0 {
            secondnumber = Int(calcText)!
        }
        else {
            firstnumber = Int(calcText)!
        }
        
        if operatorType == "+" {
            result = firstnumber + secondnumber
        } else if operatorType == "-" {
            result = firstnumber - secondnumber
        } else if operatorType == "x" {
            result = firstnumber * secondnumber
        }
        calcText = "\(result)"
    }
    
    func clear() {
        calcText = ""
        firstnumber = 0
        secondnumber = 0
        operatorType = ""
        isTypingNumber = false
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

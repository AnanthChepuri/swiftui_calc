//
//  CalcButton.swift
//  Calc
//
//  Created by Ananth Chepuri on 08/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct CalcButton {
    func calcButton(buttonText: String) -> some View {
        
        return Button(action: {
           // flag was tapped
        }) {
            Text("\(buttonText)")
                .frame(width: 80, height: 50, alignment: .center)
                .font(.headline)
                .foregroundColor(.white)
                .background(Color.green)
                .cornerRadius(14.0)
        }
    }
    
    func createCalcDigit(_ number: String) -> Button<Text> {
        return Button(action: {
            self.digitTapped(number)
        }) {
            (Text(number))
        }
    }
    
    func digitTapped(_ number: String) -> Void {
        
    }
}

struct MyButtonStyle: ButtonStyle {
    var color: Color = .green
    
    public func makeBody(configuration: MyButtonStyle.Configuration) -> some View {
        
        configuration.label
            .frame(width: 50, height: 25, alignment: .center)
            .foregroundColor(.white)
            .padding(15)
            .background(RoundedRectangle(cornerRadius: 5).fill(color))
            .compositingGroup()
            .shadow(color: .black, radius: 3)
            .opacity(configuration.isPressed ? 0.5 : 1.0)
            .scaleEffect(configuration.isPressed ? 0.8 : 1.0)
    }
}
